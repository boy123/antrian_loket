<?php 
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	public function index()
	{
		$this->load->view('antrian_online/index');
	}

	public function cari()
	{
		$no_ktp = $this->input->get('no_ktp');
		redirect("web?no_ktp=".$no_ktp."#hasilcari");
	}

	public function simpan_pendaftaran()
	{
		$id_jenis_layanan = $this->input->post('jenis_layanan');

		$max_antrian = get_data('jenis_layanan','id_jenis_layanan',$id_jenis_layanan,'max_antrian');
        if ($max_antrian == 0) {
            $max_antrian = 1000;
        }

        $id_loket = get_data('loket','id_jenis_layanan',$id_jenis_layanan,'id_loket');
        $loket = get_data('loket','id_jenis_layanan',$id_jenis_layanan,'loket');
        $id_instansi = get_data('jenis_layanan','id_jenis_layanan',$id_jenis_layanan,'id_instansi');
        $jenis_layanan = get_data('jenis_layanan','id_jenis_layanan',$id_jenis_layanan,'jenis_layanan');
        $nama_instansi = get_data('instansi','id_instansi',$id_instansi,'nama_instansi');
        $tgl = date('Y-m-d');

        // cek no antrian
        $no_antrian = 0;
        $no_atas = $this->db->query("SELECT no_antrian FROM antrian where id_loket='$id_loket' and tanggal='$tgl' ORDER BY no_antrian DESC ");
        if ($no_atas->num_rows() > 0) {
            if ($no_atas->row()->no_antrian >= $max_antrian) {
                ?>
                <script type="text/javascript">
                    alert("No Antrian sudah mencapai MAX");
                    window.location = "<?php echo base_url('web') ?>"
                </script>
                <?php
            } else {
                $no_antrian = $no_atas->row()->no_antrian + 1;
            }
        } else {
            $no_antrian = 1;
        }


        $this->db->insert('antrian', array(
            'no_antrian' => $no_antrian,
            'id_jenis_layanan' => $id_jenis_layanan,
            'id_loket' => $id_loket,
            'tanggal'=>$tgl,
            'no_ktp' => $this->input->post('no_ktp'),
            'nama' => $this->input->post('nama'),
            'no_hp' => $this->input->post('no_hp'),
            'is_online' => 'y',
            'waktu_kunjungan' => $this->input->post('waktu_kunjungan'),
            'created_at' => get_waktu()
        ));

        if ($this->db->affected_rows()) {
            ?>
            <script type="text/javascript">
                alert("No Antrian berhasil di buat");
                window.location = "<?php echo base_url('web?no_ktp='.$this->input->post('no_ktp')."#hasilcari") ?>"
            </script>
            <?php
        } else {
            ?>
            <script type="text/javascript">
                alert("Terjadi kesalahan, silahkan ulangi lagi");
                window.location = "<?php echo base_url('web#pendaftaran') ?>"
            </script>
            <?php
        }
	}

}

/* End of file Web.php */
/* Location: ./application/controllers/Web.php */