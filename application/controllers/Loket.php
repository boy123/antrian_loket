<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Loket extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Loket_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'loket/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'loket/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'loket/index.html';
            $config['first_url'] = base_url() . 'loket/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Loket_model->total_rows($q);
        $loket = $this->Loket_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'loket_data' => $loket,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'judul_page' => 'Data Loket',
            'konten' => 'loket/loket_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Loket_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_loket' => $row->id_loket,
		'loket' => $row->loket,
		'id_jenis_layanan' => $row->id_jenis_layanan,
		'keterangan' => $row->keterangan,
	    );
            $this->load->view('loket/loket_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('loket'));
        }
    }

    public function create() 
    {
        $data = array(
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'judul_page' => 'Data Loket',
            'konten' => 'loket/loket_form',
            'button' => 'Create',
            'action' => site_url('loket/create_action'),
	    'id_loket' => set_value('id_loket'),
        'loket' => set_value('loket'),
	    'suara' => set_value('suara'),
	    'id_jenis_layanan' => set_value('id_jenis_layanan'),
	    'keterangan' => set_value('keterangan'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            // $suara = upload_gambar_biasa('loket', 'audio/loket/', 'wav', 10000, 'loket');
            // if (strpos($suara, '<p>') !== false) {
            //     $this->session->set_flashdata('message', alert_biasa($suara,'error'));
            //     redirect('loket/create','refresh');
            // }

            $data = array(
		'loket' => $this->input->post('loket',TRUE),
        'id_jenis_layanan' => $this->input->post('id_jenis_layanan',TRUE),
		// 'suara' => $suara,
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Loket_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('loket'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Loket_model->get_by_id($id);

        if ($row) {
            $data = array(
                'atas' => 'page/atas',
                'bawah' => 'page/bawah',
                'judul_page' => 'Data Loket',
                'konten' => 'loket/loket_form',
                'button' => 'Update',
                'action' => site_url('loket/update_action'),
		'id_loket' => set_value('id_loket', $row->id_loket),
        'loket' => set_value('loket', $row->loket),
		'suara' => set_value('suara', $row->suara),
		'id_jenis_layanan' => set_value('id_jenis_layanan', $row->id_jenis_layanan),
		'keterangan' => set_value('keterangan', $row->keterangan),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('loket'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_loket', TRUE));
        } else {

            // $retVal = ($_FILES['suara']['name'] == '') ? $_POST['suara_old'] : upload_gambar_biasa('suara', 'audio/loket/', 'wav', 10000, 'suara');
            // if (strpos($retVal, '<p>') !== false) {
            //     $this->session->set_flashdata('message', alert_biasa($retVal,'error'));
            //     redirect('loket/update/'.$this->input->post('id_loket', TRUE),'refresh');
            // }

            $data = array(
		'loket' => $this->input->post('loket',TRUE),
        'id_jenis_layanan' => $this->input->post('id_jenis_layanan',TRUE),
		// 'suara' => $retVal,
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Loket_model->update($this->input->post('id_loket', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('loket'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Loket_model->get_by_id($id);

        if ($row) {
            $this->Loket_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('loket'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('loket'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('loket', 'loket', 'trim|required');
	$this->form_validation->set_rules('id_jenis_layanan', 'id jenis layanan', 'trim|required');
	$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');

	$this->form_validation->set_rules('id_loket', 'id_loket', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Loket.php */
/* Location: ./application/controllers/Loket.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-12-27 02:32:36 */
/* https://jualkoding.com */