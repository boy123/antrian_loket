<?php
date_default_timezone_set('Asia/Jakarta');
defined('BASEPATH') OR exit('No direct script access allowed');

use Firebase\Firebase;

class App extends CI_Controller {

    public $image = '';
    
    public function index()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }

        if ($this->session->userdata('level') == 'loket') {
            $konten = "dasboard_loket";

        } elseif($this->session->userdata('level') == 'pejabat') {

            $konten = "dasboard_pejabat";
        
        } else {
            $konten = "home_admin";
        }

        $data = array(
            'atas' => '',
            'bawah' => '',
            'konten' => $konten,
            'judul_page' => 'Dashboard',
        );
        $this->load->view('v_index', $data);
    }

    public function pengembangan()
    {
        $this->session->set_flashdata('message', alert_biasa('Sedang dalam pengembangan','info'));
        redirect("app");
    }

    public function tgl_jam_today()
    {
        $tanggal = date('Y-m-d');
        $hari = hari_id($tanggal).', '.tanggal_indo($tanggal);
        $jam = get_jam();

        echo json_encode([
            'hari' => $hari,
            'jam' => $jam
        ]);
    }

    public function antrian()
    {
        $this->load->view('antrian_onsite/f_index');
    }

    public function display()
    {
        $this->load->view('antrian_onsite/display');
    }

    public function pilih_instansi()
    {
        $this->load->view('antrian_onsite/pilih_instansi');
    }

    public function antrian_list()
    {
        $tgl = date('Y-m-d');
        $antrian_dipanggil = $this->db->query("SELECT * FROM antrian where tanggal='$tgl' and date_panggil is not null ORDER BY date_panggil DESC");
        if ($antrian_dipanggil->num_rows() > 0) {
            $diloket = get_data('loket','id_loket',$antrian_dipanggil->row()->id_loket,'loket');
            $no_antrian_dipanggil = $antrian_dipanggil->row()->no_antrian;
        } else {
            $diloket = "NOT SET";
            $no_antrian_dipanggil = "0";
        }
        $result = array(
            'diloket' => $diloket,
            'no_antrian_dipanggil' => $no_antrian_dipanggil
        );

        echo json_encode($result);
    }

    public function antrian_lainnya()
    {
        $lainnya = array();
        $tgl = date('Y-m-d');

        $this->db->select('a.loket, a.id_loket, b.jenis_layanan, c.nama_instansi');
        $this->db->from('loket a');
        $this->db->join('jenis_layanan b', 'b.id_jenis_layanan = a.id_jenis_layanan', 'inner');
        $this->db->join('instansi c', 'c.id_instansi = b.id_instansi', 'inner');
        $data_loket = $this->db->get()->result();
        foreach ($data_loket as $row) {

            //antrian berlangsung
            $berlangsung = 0;
            $antrian_now = $this->db->query("SELECT no_antrian FROM antrian where id_loket='$row->id_loket' and tanggal='$tgl' and date_panggil is not null ORDER BY no_antrian DESC");
            if ($antrian_now->num_rows() > 0) {
                $berlangsung = $antrian_now->row()->no_antrian;
            }


            //cek sisa antrian belum di panggil
            $sisa = 0;
            $cek_sisa = $this->db->query("SELECT no_antrian FROM antrian where id_loket='$row->id_loket' and tanggal='$tgl' and date_panggil is null ");
            if ($cek_sisa->num_rows() > 0) {
                $sisa = $cek_sisa->num_rows();
            }

            $selanjutnya = "-";
            if ($sisa > 0 AND $berlangsung > 0) {
                $selanjutnya = $berlangsung + 1;
            }
            
            array_push($lainnya, array(
                'id_loket' => $row->id_loket,
                'loket' => $row->loket,
                'instansi' => $row->nama_instansi,
                'jenis_layanan' => $row->jenis_layanan,
                'berlangsung' => ($berlangsung == "0") ? '-' : $berlangsung,
                'selanjutnya' => $selanjutnya,
                'sisa' => ($sisa == "0") ? '-' : $sisa
            ));

        }

        echo json_encode($lainnya);

    }

    public function cetak_antrian($id_jenis_layanan)
    {

        $max_antrian = get_data('jenis_layanan','id_jenis_layanan',$id_jenis_layanan,'max_antrian');
        if ($max_antrian == 0) {
            $max_antrian = 1000;
        }

        $id_loket = get_data('loket','id_jenis_layanan',$id_jenis_layanan,'id_loket');
        $loket = get_data('loket','id_jenis_layanan',$id_jenis_layanan,'loket');
        $id_instansi = get_data('jenis_layanan','id_jenis_layanan',$id_jenis_layanan,'id_instansi');
        $jenis_layanan = get_data('jenis_layanan','id_jenis_layanan',$id_jenis_layanan,'jenis_layanan');
        $nama_instansi = get_data('instansi','id_instansi',$id_instansi,'nama_instansi');
        $tgl = date('Y-m-d');

        // cek no antrian
        $no_antrian = 0;
        $no_atas = $this->db->query("SELECT no_antrian FROM antrian where id_loket='$id_loket' and tanggal='$tgl' ORDER BY no_antrian DESC ");
        if ($no_atas->num_rows() > 0) {
            if ($no_atas->row()->no_antrian >= $max_antrian) {
                ?>
                <script type="text/javascript">
                    alert("No Antrian sudah mencapai MAX");
                    window.location = "<?php echo base_url('app/pilih_instansi') ?>"
                </script>
                <?php
            } else {
                $no_antrian = $no_atas->row()->no_antrian + 1;
            }
        } else {
            $no_antrian = 1;
        }

        //cek sisa antrian belum di panggil
        $sisa = 0;
        $cek_sisa = $this->db->query("SELECT no_antrian FROM antrian where id_loket='$id_loket' and tanggal='$tgl' and date_panggil is null ");
        if ($cek_sisa->num_rows() > 0) {
            $sisa = $cek_sisa->num_rows();
        }

        $this->db->insert('antrian', array(
            'no_antrian' => $no_antrian,
            'id_jenis_layanan' => $id_jenis_layanan,
            'id_loket' => $id_loket,
            'tanggal'=>$tgl,
            'created_at' => get_waktu()
        ));

        if ($this->db->affected_rows()) {
            $data = array(
                'no_antrian' => $no_antrian,
                'instansi' => $nama_instansi,
                'jenis_layanan' => $jenis_layanan,
                'loket'=> $loket,
                'sisa'=> $sisa
            );

            $this->load->view('antrian_onsite/cetak', $data);
        } else {
            ?>
            <script type="text/javascript">
                alert("Terjadi kesalahan");
                window.location = "<?php echo base_url('app/pilih_instansi') ?>"
            </script>
            <?php
        }

        
    }

    public function next_antrian($id_loket)
    {
        $tgl = date('Y-m-d');
        $cek_sisa = $this->db->query("SELECT no_antrian FROM antrian where id_loket='$id_loket' and tanggal='$tgl' and date_panggil is null ORDER BY no_antrian ASC ");
        if ($cek_sisa->num_rows() > 0) {
            $this->db->where('no_antrian', $cek_sisa->row()->no_antrian);
            $this->db->where('id_loket', $id_loket);
            $this->db->where('tanggal', $tgl);
            $this->db->update('antrian', ['date_panggil'=>get_waktu()]);
            redirect("app");
        } else {
            ?>
            <script type="text/javascript">
                alert("Tidak ada No Antrian yang bisa di proses !");
                window.location = "<?php echo base_url('app') ?>";
            </script>
            <?php
        }
    }

    public function fb()
    {
        // log_r(PHP_VERSION_ID);
        $fb = Firebase::initialize('https://crud-web-be585-default-rtdb.asia-southeast1.firebasedatabase.app/', 'AIzaSyAkzFs2A4XPJP7GnOfOq-XgkekECyqUMa0');
        $a = $fb->get('/users');
        echo json_encode($a);
    }

    

    

    
}
