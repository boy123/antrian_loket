<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Waktu_kunjungan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Waktu_kunjungan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'waktu_kunjungan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'waktu_kunjungan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'waktu_kunjungan/index.html';
            $config['first_url'] = base_url() . 'waktu_kunjungan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Waktu_kunjungan_model->total_rows($q);
        $waktu_kunjungan = $this->Waktu_kunjungan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'waktu_kunjungan_data' => $waktu_kunjungan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'judul_page' => 'Data Master Kunjungan',
            'konten' => 'waktu_kunjungan/waktu_kunjungan_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Waktu_kunjungan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_waktu_kunjungan' => $row->id_waktu_kunjungan,
		'waktu_kunjungan' => $row->waktu_kunjungan,
	    );
            $this->load->view('waktu_kunjungan/waktu_kunjungan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('waktu_kunjungan'));
        }
    }

    public function create() 
    {
        $data = array(
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'judul_page' => 'Data Master Kunjungan',
            'konten' => 'waktu_kunjungan/waktu_kunjungan_form',
            'button' => 'Create',
            'action' => site_url('waktu_kunjungan/create_action'),
	    'id_waktu_kunjungan' => set_value('id_waktu_kunjungan'),
	    'waktu_kunjungan' => set_value('waktu_kunjungan'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'waktu_kunjungan' => $this->input->post('waktu_kunjungan',TRUE),
	    );

            $this->Waktu_kunjungan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('waktu_kunjungan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Waktu_kunjungan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'atas' => 'page/atas',
                'bawah' => 'page/bawah',
                'judul_page' => 'Data Master Kunjungan',
                'konten' => 'waktu_kunjungan/waktu_kunjungan_form',
                'button' => 'Update',
                'action' => site_url('waktu_kunjungan/update_action'),
		'id_waktu_kunjungan' => set_value('id_waktu_kunjungan', $row->id_waktu_kunjungan),
		'waktu_kunjungan' => set_value('waktu_kunjungan', $row->waktu_kunjungan),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('waktu_kunjungan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_waktu_kunjungan', TRUE));
        } else {
            $data = array(
		'waktu_kunjungan' => $this->input->post('waktu_kunjungan',TRUE),
	    );

            $this->Waktu_kunjungan_model->update($this->input->post('id_waktu_kunjungan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('waktu_kunjungan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Waktu_kunjungan_model->get_by_id($id);

        if ($row) {
            $this->Waktu_kunjungan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('waktu_kunjungan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('waktu_kunjungan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('waktu_kunjungan', 'waktu kunjungan', 'trim|required');

	$this->form_validation->set_rules('id_waktu_kunjungan', 'id_waktu_kunjungan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Waktu_kunjungan.php */
/* Location: ./application/controllers/Waktu_kunjungan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-12-28 03:44:30 */
/* https://jualkoding.com */