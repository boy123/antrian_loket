<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Instansi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Instansi_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'instansi/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'instansi/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'instansi/index.html';
            $config['first_url'] = base_url() . 'instansi/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Instansi_model->total_rows($q);
        $instansi = $this->Instansi_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'instansi_data' => $instansi,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'judul_page' => 'Data Instansi',
            'konten' => 'instansi/instansi_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Instansi_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_instansi' => $row->id_instansi,
		'nama_instansi' => $row->nama_instansi,
		'alamat' => $row->alamat,
		'website' => $row->website,
		'logo' => $row->logo,
		'keterangan' => $row->keterangan,
	    );
            $this->load->view('instansi/instansi_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('instansi'));
        }
    }

    public function create() 
    {
        $data = array(
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'judul_page' => 'Data Instansi',
            'konten' => 'instansi/instansi_form',
            'button' => 'Create',
            'action' => site_url('instansi/create_action'),
	    'id_instansi' => set_value('id_instansi'),
	    'nama_instansi' => set_value('nama_instansi'),
	    'alamat' => set_value('alamat'),
	    'website' => set_value('website'),
	    'logo' => set_value('logo'),
	    'keterangan' => set_value('keterangan'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $img = upload_gambar_biasa('logo', 'image/logo/', 'jpg|png|jpeg|gif', 10000, 'logo');
            if (strpos($img, '<p>') !== false) {
                $this->session->set_flashdata('message', alert_biasa($img,'error'));
                redirect('instansi/create','refresh');
            }
            
            $data = array(
		'nama_instansi' => $this->input->post('nama_instansi',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'website' => $this->input->post('website',TRUE),
		'logo' => $img,
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Instansi_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('instansi'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Instansi_model->get_by_id($id);

        if ($row) {
            $data = array(
                'atas' => 'page/atas',
                'bawah' => 'page/bawah',
                'judul_page' => 'Data Instansi',
                'konten' => 'instansi/instansi_form',
                'button' => 'Update',
                'action' => site_url('instansi/update_action'),
		'id_instansi' => set_value('id_instansi', $row->id_instansi),
		'nama_instansi' => set_value('nama_instansi', $row->nama_instansi),
		'alamat' => set_value('alamat', $row->alamat),
		'website' => set_value('website', $row->website),
		'logo' => set_value('logo', $row->logo),
		'keterangan' => set_value('keterangan', $row->keterangan),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('instansi'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_instansi', TRUE));
        } else {

            $retVal = ($_FILES['logo']['name'] == '') ? $_POST['logo_old'] : upload_gambar_biasa('logo', 'image/logo/', 'jpg|png|jpeg', 10000, 'logo');
            if (strpos($retVal, '<p>') !== false) {
                $this->session->set_flashdata('message', alert_biasa($retVal,'error'));
                redirect('instansi/update/'.$this->input->post('id_instansi', TRUE),'refresh');
            }

            $data = array(
		'nama_instansi' => $this->input->post('nama_instansi',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'website' => $this->input->post('website',TRUE),
		'logo' => $retVal,
		'keterangan' => $this->input->post('keterangan',TRUE),
	    );

            $this->Instansi_model->update($this->input->post('id_instansi', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('instansi'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Instansi_model->get_by_id($id);

        if ($row) {
            $this->Instansi_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('instansi'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('instansi'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('nama_instansi', 'nama instansi', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('website', 'website', 'trim|required');
	$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');

	$this->form_validation->set_rules('id_instansi', 'id_instansi', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Instansi.php */
/* Location: ./application/controllers/Instansi.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2021-12-27 02:32:13 */
/* https://jualkoding.com */