
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Loket <?php echo form_error('loket') ?></label>
            <input type="text" class="form-control" name="loket" id="loket" placeholder="Loket" value="<?php echo $loket; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Jenis Layanan <?php echo form_error('id_jenis_layanan') ?></label>
            <select name="id_jenis_layanan" class="form-control select2">
                <option value="<?php echo $id_jenis_layanan ?>"><?php echo get_data('jenis_layanan','id_jenis_layanan',$id_jenis_layanan,'jenis_layanan') ?></option>
                <?php foreach ($this->db->query("SELECT * from jenis_layanan where id_jenis_layanan not in(select id_jenis_layanan from loket) ")->result() as $rw): ?>
                    <option value="<?php echo $rw->id_jenis_layanan ?>"><?php echo $rw->jenis_layanan ?></option>
                <?php endforeach ?>
            </select>
        </div>
        <!-- <div class="form-group">
            <label>Suara Loket</label>
            <input type="file" name="suara" class="form-control" required>
            <input type="hidden" name="suara_old" value="<?php echo $suara ?>">
            <p>
                <?php if ($suara != ''): ?>
                    *) suara sebelumnya <br>
                    <audio controls>
                      <source src="audio/loket/<?php echo $suara ?>" type="audio/wav">
                    Your browser does not support the audio element.
                    </audio>
                <?php endif ?>
                
            </p>
        </div> -->
	    <div class="form-group">
            <label for="keterangan">Keterangan <?php echo form_error('keterangan') ?></label>
            <textarea class="form-control" rows="3" name="keterangan" id="keterangan" placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
        </div>
	    <input type="hidden" name="id_loket" value="<?php echo $id_loket; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('loket') ?>" class="btn btn-default">Cancel</a>
	</form>
   