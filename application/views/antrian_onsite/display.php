<?php $this->load->view('antrian_onsite/atas'); ?>

<body class="animsition page-login layout-full page-dark"
     style="background-image:url('front/antrian_onsite/assets/img/lockscreen.jpg'); background-size:cover;background-position: center;">

      <!-- Page -->
     <div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">

          <div class="page-content">
               <?php $this->load->view('antrian_onsite/header'); ?>

               <section style="margin-top: 20px">
                    <div class="row">
                         <div class="col-md-4">
                              <div class="alert alert-success" style="text-align: center; height:400px">
                                   <h2 style="font-family: Impact; font-size: 50px">Nomor Antrian</h2>
                                   <h1 style="font-family: Impact; font-size: 150px" id="no_antrian_dipanggil">0</h1>
                                   <h3 style="font-family: Impact; font-size: 40px;" id="diloket">Loket NOT SET</h3>
                              </div>
                         </div>

                         <div class="col-md-8">
                             <ul class="bxslider">
                                <?php foreach ($this->db->get('slide')->result() as $rw): ?>
                                   <li><img src="image/slide/<?php echo $rw->slide ?>"/></li>
                                <?php endforeach ?>
                              </ul>
                         </div>
                    </div>

                    <div class="row">

                         <div class="col-md-12">
                              <div id="lainnya"></div>
                         </div>
                         
                    </div>
                    
                    <audio id="suarabel" src="<?php echo base_url('audio/Airport_Bell.mp3'); ?>"></audio>
               </section>



          </div>
     </div>
     <!-- End Page -->


     <!-- Core  -->
     <?php $this->load->view('antrian_onsite/footer'); ?>

