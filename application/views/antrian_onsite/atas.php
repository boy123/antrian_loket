<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <title>Aplikasi Antrian Online DPMPTSP Kota Tebing Tinggi</title>
     <meta name="description" content="Aplikasi Antrian" />
     <meta name="keywords" content="Aplikasi Antrian, Mal Pelayanan Publik Kota Tebing Tinggi" />
     <meta name="author" content="Mal Pelayanan Publik Kota Tebing Tinggi " />
     <base href="<?php echo base_url() ?>">
     <!-- Favicon -->
     <!-- <link rel="shortcut icon" href="favicon.ico"> -->
     <!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> -->

     <!-- Stylesheets -->
     <link rel="stylesheet" href="front/antrian_onsite/global/css/bootstrap.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/css/bootstrap-extend.css">
     <link rel="stylesheet" href="front/antrian_onsite/assets/css/site.css">

     <!-- Plugins -->
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/animsition/animsition.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/asscrollable/asScrollable.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/switchery/switchery.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/intro-js/introjs.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/slidepanel/slidePanel.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/jquery-mmenu/jquery-mmenu.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/flag-icon-css/flag-icon.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/waves/waves.css">
     <link rel="stylesheet" href="front/antrian_onsite/assets/examples/css/pages/login.css">

     <link rel="stylesheet" href="front/antrian_onsite/owlcarousel/assets/owl.carousel.min.css">
     <link rel="stylesheet" href="front/antrian_onsite/owlcarousel/assets/owl.theme.default.min.css">

     <link href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css" rel="stylesheet" />


     <!-- Fonts -->
     <link rel="stylesheet" href="front/antrian_onsite/global/fonts/material-design/material-design.min.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/fonts/brand-icons/brand-icons.min.css">
     <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

     <!-- Scripts -->
     <script src="front/antrian_onsite/global/vendor/breakpoints/breakpoints.js"></script>
     <script>
     Breakpoints();
     </script>
</head>