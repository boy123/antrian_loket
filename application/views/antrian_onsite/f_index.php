<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <title>Aplikasi Antrian</title>
     <meta name="description" content="Aplikasi Antrian" />
     <meta name="keywords" content="Aplikasi Antrian, MAL PELAYANAN PUBLIK KOTA PEKANBARU" />
     <meta name="author" content="MAL PELAYANAN PUBLIK KOTA PEKANBARU" />
     <base href="<?php echo base_url() ?>">
     <!-- Favicon -->
     <link rel="shortcut icon" href="favicon.ico">
     <link rel="icon" href="favicon.ico" type="image/x-icon">

     <!-- Stylesheets -->
     <link rel="stylesheet" href="front/antrian_onsite/global/css/bootstrap.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/css/bootstrap-extend.css">
     <link rel="stylesheet" href="front/antrian_onsite/assets/css/site.css">

     <!-- Plugins -->
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/animsition/animsition.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/asscrollable/asScrollable.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/switchery/switchery.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/intro-js/introjs.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/slidepanel/slidePanel.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/jquery-mmenu/jquery-mmenu.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/flag-icon-css/flag-icon.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/vendor/waves/waves.css">
     <link rel="stylesheet" href="front/antrian_onsite/assets/examples/css/pages/login.css">


     <!-- Fonts -->
     <link rel="stylesheet" href="front/antrian_onsite/global/fonts/material-design/material-design.min.css">
     <link rel="stylesheet" href="front/antrian_onsite/global/fonts/brand-icons/brand-icons.min.css">
     <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

     <!-- Scripts -->
     <script src="front/antrian_onsite/global/vendor/breakpoints/breakpoints.js"></script>
     <script>
     Breakpoints();
     </script>
</head>

<body class="animsition page-login layout-full page-dark"
     style="background-image:url('front/antrian_onsite/assets/img/lockscreen.jpg');background-size:cover;background-position: center;">

     <!-- Page -->
     <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
          <div class="page-content vertical-align-middle">
               <div class="brand" style="margin-bottom:10%;">
                    <img width="100%" src="front/antrian_onsite/assets/img/mpp_logo.png">
               </div>

               <style>
               #inputName,
               #inputPassword {
                    font-weight: 700;
                    color: #ffff;
               }
               </style>
               <form action="https://antrian.mpp.pekanbaru.go.id/?page=login" method="POST" style="width:100%;">
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                         <input type="text" class="form-control empty" id="inputName" autocomplete="off" name="username"
                              required>
                         <label class="floating-label" for="inputName">Username</label>
                    </div>
                    <div class="form-group form-material floating" data-plugin="formMaterial">
                         <input type="password" class="form-control empty" id="inputPassword" name="password" required>
                         <label class="floating-label" for="inputPassword">Password</label>
                    </div>
                    <button type="submit" class="btn btn-animate btn-animate-side btn-primary btn-block">
                         <span><i class="icon md-forward" aria-hidden="true"></i>LOGIN</span>
                    </button>
               </form>

               <footer class="page-copyright page-copyright-inverse">
                    <p>MPP Kota Pekanbaru - Jl. Jenderal Sudirman No.464</p>
                    <div class="social">
                         <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                              <i class="icon bd-instagram" aria-hidden="true"></i>
                         </a>
                         <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                              <i class="icon bd-twitter" aria-hidden="true"></i>
                         </a>
                         <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                              <i class="icon bd-facebook" aria-hidden="true"></i>
                         </a>
                         <a class="btn btn-icon btn-pure" href="javascript:void(0)">
                              <i class="icon bd-google-plus" aria-hidden="true"></i>
                         </a>
                    </div>
               </footer>
          </div>
     </div>
     <!-- End Page -->


     <!-- Core  -->
     <script src="front/antrian_onsite/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
     <script src="front/antrian_onsite/global/vendor/jquery/jquery.js"></script>
     <script src="front/antrian_onsite/global/vendor/popper-js/umd/popper.min.js"></script>
     <script src="front/antrian_onsite/global/vendor/bootstrap/bootstrap.js"></script>
     <script src="front/antrian_onsite/global/vendor/animsition/animsition.js"></script>
     <script src="front/antrian_onsite/global/vendor/mousewheel/jquery.mousewheel.js"></script>
     <script src="front/antrian_onsite/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
     <script src="front/antrian_onsite/global/vendor/asscrollable/jquery-asScrollable.js"></script>
     <script src="front/antrian_onsite/global/vendor/waves/waves.js"></script>

     <!-- Plugins -->
     <script src="front/antrian_onsite/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
     <script src="front/antrian_onsite/global/vendor/switchery/switchery.js"></script>
     <script src="front/antrian_onsite/global/vendor/intro-js/intro.js"></script>
     <script src="front/antrian_onsite/global/vendor/screenfull/screenfull.js"></script>
     <script src="front/antrian_onsite/global/vendor/slidepanel/jquery-slidePanel.js"></script>
     <script src="front/antrian_onsite/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>

     <!-- Scripts -->
     <script src="front/antrian_onsite/global/js/Component.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin.js"></script>
     <script src="front/antrian_onsite/global/js/Base.js"></script>
     <script src="front/antrian_onsite/global/js/Config.js"></script>

     <script src="front/antrian_onsite/assets/js/Section/Menubar.js"></script>
     <script src="front/antrian_onsite/assets/js/Section/Sidebar.js"></script>
     <script src="front/antrian_onsite/assets/js/Section/PageAside.js"></script>
     <script src="front/antrian_onsite/assets/js/Section/GridMenu.js"></script>

     <!-- Config -->
     <script src="front/antrian_onsite/global/js/config/colors.js"></script>
     <script src="front/antrian_onsite/assets/js/config/tour.js"></script>
     <script>
     Config.set('assets', 'assets');
     </script>

     <!-- Page -->
     <script src="front/antrian_onsite/assets/js/Site.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin/asscrollable.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin/slidepanel.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin/switchery.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin/jquery-placeholder.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin/material.js"></script>

     <script>
     (function(document, window, $) {
          'use strict';

          var Site = window.Site;
          $(document).ready(function() {
               Site.run();
          });
     })(document, window, jQuery);
     </script>

</body>

</html>