<?php $this->load->view('antrian_onsite/atas'); ?>

<body class="animsition page-login layout-full page-dark"
     style="background-image:url('front/antrian_onsite/assets/img/lockscreen.jpg'); background-size:cover;background-position: center;">

     <!-- Page -->
     <div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">

          <div class="page-content">
               <?php $this->load->view('antrian_onsite/header'); ?>

               <section style="margin-top: 100px">
                    <div class="row">
                         <?php foreach ($this->db->get('instansi')->result() as $rw): ?>
                         <div class="col-md-3">
                              <a data-toggle="modal" data-target="#idModal<?php echo $rw->id_instansi ?>">
                                   <div class="alert alert-success" style="text-align: center;">
                                        <table style="text-align: left;">
                                             <tr>
                                                  <td><img src="image/logo/<?php echo $rw->logo ?>"
                                                            style="width: 50px;"></td>
                                                  <td style="padding-left: 10px;">
                                                       <h5><?php echo $rw->nama_instansi ?></h5>
                                                       <?php echo $rw->keterangan ?>
                                                  </td>
                                             </tr>

                                        </table>
                                   </div>
                              </a>
                         </div>

                         <!-- Modal -->
                         <div class="modal fade" id="idModal<?php echo $rw->id_instansi ?>" role="dialog">
                              <div class="modal-dialog modal-lg">
                                   <div class="modal-content">
                                        <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                                             <h4 class="modal-title">Pilih Jenis Layanan</h4>
                                        </div>
                                        <div class="modal-body">
                                             <center>
                                                  <img src="image/logo/<?php echo $rw->logo ?>" style="width: 80px;">
                                                  <h4><?php echo $rw->nama_instansi ?></h4>
                                             </center>


                                             <div class="row">
                                                  <?php foreach ($this->db->get_where('jenis_layanan', ['id_instansi' => $rw->id_instansi])->result() as $br): ?>
                                                  <div class="col-md-6">
                                                       <a href="app/cetak_antrian/<?php echo $br->id_jenis_layanan ?>" class="btn btn-info btn-block" style="margin-top: 5px; margin-right: 5px; color: white;"><?php echo $br->jenis_layanan ?></a>
                                                  </div>
                                                       
                                                  <?php endforeach ?>
                                             </div>


                                        </div>
                                        <div class="modal-footer">
                                             <button type="button" class="btn btn-danger"
                                                  data-dismiss="modal">Close</button>
                                        </div>
                                   </div>
                              </div>
                         </div>
                         <?php endforeach ?>
                    </div>


               </section>



          </div>
     </div>
     <!-- End Page -->


     <!-- Core  -->
     <?php $this->load->view('antrian_onsite/footer'); ?>