<script src="front/antrian_onsite/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
     <script src="front/antrian_onsite/global/vendor/jquery/jquery.js"></script>
     <script src="front/antrian_onsite/global/vendor/popper-js/umd/popper.min.js"></script>
     <script src="front/antrian_onsite/global/vendor/bootstrap/bootstrap.js"></script>
     <script src="front/antrian_onsite/global/vendor/animsition/animsition.js"></script>
     <script src="front/antrian_onsite/global/vendor/mousewheel/jquery.mousewheel.js"></script>
     <script src="front/antrian_onsite/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
     <script src="front/antrian_onsite/global/vendor/asscrollable/jquery-asScrollable.js"></script>
     <script src="front/antrian_onsite/global/vendor/waves/waves.js"></script>

     <!-- Plugins -->
     <script src="front/antrian_onsite/global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
     <script src="front/antrian_onsite/global/vendor/switchery/switchery.js"></script>
     <script src="front/antrian_onsite/global/vendor/intro-js/intro.js"></script>
     <script src="front/antrian_onsite/global/vendor/screenfull/screenfull.js"></script>
     <script src="front/antrian_onsite/global/vendor/slidepanel/jquery-slidePanel.js"></script>
     <script src="front/antrian_onsite/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>

     <!-- Scripts -->
     <script src="front/antrian_onsite/global/js/Component.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin.js"></script>
     <script src="front/antrian_onsite/global/js/Base.js"></script>
     <script src="front/antrian_onsite/global/js/Config.js"></script>

     <script src="front/antrian_onsite/assets/js/Section/Menubar.js"></script>
     <script src="front/antrian_onsite/assets/js/Section/Sidebar.js"></script>
     <script src="front/antrian_onsite/assets/js/Section/PageAside.js"></script>
     <script src="front/antrian_onsite/assets/js/Section/GridMenu.js"></script>

     <!-- Config -->
     <script src="front/antrian_onsite/global/js/config/colors.js"></script>
     <script src="front/antrian_onsite/assets/js/config/tour.js"></script>
     <script>
     Config.set('assets', 'assets');
     </script>

     <!-- Page -->
     <script src="front/antrian_onsite/assets/js/Site.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin/asscrollable.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin/slidepanel.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin/switchery.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin/jquery-placeholder.js"></script>
     <script src="front/antrian_onsite/global/js/Plugin/material.js"></script>
     <script src="front/antrian_onsite/owlcarousel/owl.carousel.min.js"></script>

     <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js"></script>

     <script src="https://www.gstatic.com/firebasejs/7.15.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.15.0/firebase-database.js"></script>
    <script src="https://code.responsivevoice.org/responsivevoice.js?key=FMTzyQ5N"></script>
    


     <script>


    var firebaseConfig = {
        apiKey: "AIzaSyAkzFs2A4XPJP7GnOfOq-XgkekECyqUMa0",
        authDomain: "crud-web-be585.firebaseapp.com",
        databaseURL:
          "https://crud-web-be585-default-rtdb.asia-southeast1.firebasedatabase.app/",
        projectId: "crud-web-be585",
        storageBucket: "crud-web-be585.appspot.com",
        messagingSenderId: "214105427344",
        appId: "1:214105427344:web:34a24e2b574a572f8a3715",
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
      var database = firebase.database();


    database.ref("notifikasi").on("child_changed", function (data) {
          console.log("ada perubahan ");
          get_antrian_list();
            get_antrian_lainnya();
      });
      

    
    function get_hari_today() {
        $.ajax({
            url: 'app/tgl_jam_today',
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            $("#tanggal_header").text(data.hari);
            $("#jam_header").text(data.jam);
        })
        .fail(function() {
            console.log("error");
        })
        
        
    }

    function get_antrian_list() {
        $.ajax({
            url: 'app/antrian_list',
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            $("#no_antrian_dipanggil").text(data.no_antrian_dipanggil);
            $("#diloket").text(data.diloket);

            document.getElementById("suarabel").pause();
            document.getElementById("suarabel").currentTime=0;
            document.getElementById("suarabel").play();

            //set delay
            totalWaktu = document.getElementById("suarabel").duration*1000;

            setTimeout(function() {
                var text = "nomor antrian "+data.no_antrian_dipanggil+", silahkan ke "+data.diloket;
                responsiveVoice.speak(text, 'Indonesian Female', {
                    rate: 0.6
                });
            }, 7000);

            

            
        })
        .fail(function() {
            console.log("error");
        })
    }

    function get_antrian_lainnya() {
        $.ajax({
            url: 'app/antrian_lainnya',
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {

            $('#lainnya').html('<div id="testing" class="owl-carousel"></div>');

            $.each(data, function(i, row) {
                $(".owl-carousel").append('<div class="alert alert-warning" style="text-align: center;">'+
                              '<h3>'+row.loket+'</h2>'+
                              '<h2>'+row.berlangsung+'</h1>'+
                              '<h4>'+row.instansi+'</h3>'+
                              
                         '<hr>'+
                         '<div class="row">'+
                              '<div class="col-md-6">'+
                                   '<b style="font-size: 20px">'+row.selanjutnya+'</b>'+
                                   '<p>SELANJUTNYA</p>'+
                              '</div>'+
                              '<div class="col-md-6">'+
                                   '<b style="font-size: 20px">'+row.sisa+'</b>'+
                                   '<p>SISA</p>'+
                              '</div>'+
                         '</div>'+
                    '</div>');
            });

            var owl = $("#testing");
            owl.owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                autoplay: true,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                responsive:{
                   0:{
                       items:1
                   },
                   600:{
                       items:3
                   },
                   1000:{
                       items:7
                   }
                }
            });

        })
        .fail(function() {
            console.log("error");
        })
    }

     (function(document, window, $) {
          'use strict';

          var Site = window.Site;
          $(document).ready(function() {
               Site.run();      

               $('.bxslider').bxSlider({
                    auto: true,
                  autoControls: true,
                  stopAutoOnClick: true,
                  pager: true,
                  slideWidth: 600
               });         
               
                <?php if ($this->uri->segment(2) == 'display'): ?>
                    get_antrian_list();
                    get_antrian_lainnya();
                <?php endif ?>

                setInterval(function(){
                    get_hari_today();

                },1000);

                



          });
     })(document, window, jQuery);
     </script>

</body>

</html>