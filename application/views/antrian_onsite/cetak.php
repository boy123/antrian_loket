<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <base href="<?php echo base_url() ?>">
    <link rel="stylesheet" href="front/antrian_onsite/global/css/bootstrap.css">
    <style type="text/css">
         @media print
        {
            @page
            {
                size: 3.54in 1.57in;
                size: portrait;
            }
        }
        body{
            height:90mm;
            width:90mm;


        }
    </style>
</head>
<body>

<div class="row">
    <div class="col-md-12">
        <img width="100%" src="front/antrian_onsite/assets/img/mpp_logo.png">
    </div>
</div>
<div class="row" style="margin-top: 10px">
    <div class="col-md-12">
        <center>
            
            <h5><?php echo hari_id(date('Y-m-d')).', '.tanggal_indo(date('Y-m-d')).' - '.get_jam() ?></h5>
            <h3>Nomor Antrian</h3>
            <h1 style="font-size: 70px"><?php echo $no_antrian ?></h1>


        </center>
        <div>
            <h5>Instansi : <?php echo $instansi ?> </h5>
            <h5>Jenis Layanan : <?php echo $jenis_layanan ?></h5>
            <h5>Sisa Antrian : <?php echo $sisa ?></h5>
        </div>
        <center>
            <h1 style="font-size: 50px"><?php echo $loket ?></h1>
        </center>
        <hr style="border: solid 1px black;">
        <center>
            <h6 >"Budayakan Antri Untuk Kenyamanan Bersama"</h6  >
            <h6 >Terima Kasih Atas Kunjungan Anda</h6    >
        </center>
    </div>
</div>

<script type="text/javascript">print(); location="<?php echo base_url('app/pilih_instansi'); ?>"</script>

</body>
</html>