
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Nama Instansi <?php echo form_error('nama_instansi') ?></label>
            <input type="text" class="form-control" name="nama_instansi" id="nama_instansi" placeholder="Nama Instansi" value="<?php echo $nama_instansi; ?>" />
        </div>
	    <div class="form-group">
            <label for="alamat">Alamat <?php echo form_error('alamat') ?></label>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Website <?php echo form_error('website') ?></label>
            <input type="text" class="form-control" name="website" id="website" placeholder="Website" value="<?php echo $website; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Logo </label>
            <input type="file" class="form-control" name="logo" />
            <input type="hidden" name="logo_old" value="<?php echo $logo ?>">
            <p>
                <?php if ($logo != ''): ?>
                    *) Gambar sebelumnya <br>
                    <img src="image/logo/<?php echo $logo ?>" style="width: 50px;">
                <?php endif ?>
                
            </p>
        </div>
	    <div class="form-group">
            <label for="keterangan">Keterangan <?php echo form_error('keterangan') ?></label>
            <textarea class="form-control" rows="3" name="keterangan" id="keterangan" placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
        </div>
	    <input type="hidden" name="id_instansi" value="<?php echo $id_instansi; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('instansi') ?>" class="btn btn-default">Cancel</a>
	</form>
   