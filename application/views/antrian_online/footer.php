<footer id="footer">
    <div class="footer-top">
      <div class="container">
        
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; <?php echo date('Y') ?> Copyright <strong><span>MPP Tebing Tinggi</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
       
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="bx bxs-up-arrow-alt"></i></a>

  <!-- Vendor JS Files -->
  <script src="front/antrian_online/vendor/jquery/jquery.min.js"></script>
  <script src="front/antrian_online/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="front/antrian_online/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="front/antrian_online/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="front/antrian_online/vendor/venobox/venobox.min.js"></script>
  <script src="front/antrian_online/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="front/antrian_online/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="front/antrian_online/js/main.js"></script>