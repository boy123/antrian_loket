<header id="header" class="fixed-top">
    <div class="container d-flex">

      <div class="logo mr-auto">
        <h1 class="text-light"><a href=""><span>MPP Tebing Tinggi</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="front/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="active"><a href="">Beranda</a></li>
          <li><a href="#cekantrian">Cek Antrian</a></li>
          <li><a href="#pendaftaran">Pendaftaran No Antrian</a></li>
          <li><a href="login">Login</a></li>

        </ul>
      </nav><!-- .nav-menu -->

      <div class="header-social-links">
        
      </div>

    </div>
  </header><!-- End Header -->