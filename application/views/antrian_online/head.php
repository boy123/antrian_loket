<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <base href="<?php echo base_url() ?>">

  <title>Aplikasi Antrian</title>
  <meta name="description" content="Aplikasi Antrian" />
   <meta name="keywords" content="Aplikasi Antrian, MAL PELAYANAN PUBLIK KOTA PEKANBARU" />
   <meta name="author" content="MAL PELAYANAN PUBLIK KOTA PEKANBARU" />

  <!-- Favicons -->
  <link href="front/antrian_online/img/favicon.png" rel="icon">
  <link href="front/antrian_online/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="front/antrian_online/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
  <link rel="stylesheet" href="assets/bower_components/font-awesome/css/font-awesome.min.css">
  <link href="front/antrian_online/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="front/antrian_online/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="front/antrian_online/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="front/antrian_online/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="front/antrian_online/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="front/antrian_online/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Scaffold - v2.2.1
  * Template URL: https://bootstrapmade.com/scaffold-bootstrap-metro-style-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>