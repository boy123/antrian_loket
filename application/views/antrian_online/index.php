<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('antrian_online/head'); ?>

<body>

  <!-- ======= Header ======= -->
  <?php $this->load->view('antrian_online/header'); ?>

  <!-- ======= Hero Section ======= -->
  <section id="hero">

    <div class="container">
      <div class="row">
        <div class="col-lg-12 order-1 order-lg-2 hero-img" data-aos="fade-left">
          <img src="front/antrian_online/img/antri.jpg" class="img-fluid" alt="">
          <h4>Memberikan Kemudahan dalam melakukan antrian online dimanapun.</h4>
        </div>
        
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

     <!-- ======= CEK JADWAL Section ======= -->
    <section id="cekantrian" class="pricing section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
    
          </br>
          <h2>Cek No Antrian</h2>
          <p>Isi NIK Anda untuk melihat No Antrian</p>
          <br>
        </div>


        <div class="row">
          
          <div class="col-md-12">
            <form action="web/cari" method="GET">
              <div class="form-group">
                <label for="name">Nomor Induk Kependudukan (NIK)</label>
                <input type="number" pattern="[0-9]+" oninput="if(value.length>16)value=value.slice(0,16)"  class="form-control" name="no_ktp" placeholder="Masukkan NIK Anda" required />
              </div>
              <div class="form-group">
                <button class="btn btn-primary">Cek No Antrian</button>
              </div>

            </form>
          </div>

          
          
        </div>

      </div>
    </section>
    <!-- CEK JADWAL Section -->


    <section id="hasilcari" class="contact section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>No Antrian Anda</h2>
        </div>

        <div class="row">

          <?php 
            if (isset($_GET['no_ktp'])): 

              date_default_timezone_set('Asia/Jakarta');

              $no_ktp = $this->input->get('no_ktp');
              $this->db->where('tanggal', date('Y-m-d'));
              $this->db->where('no_ktp', $no_ktp);
              $antrian = $this->db->get('antrian');
              ?>


              <?php if ($antrian->num_rows() > 0): ?>

                <center>
                  <h2>Hasil Pencarian</h2>
                </center>

                <table class="table table-bordered table-stripped">
                  <tr>
                    <td>No Antrian</td>
                    
                    <td><h2><?php echo $antrian->row()->no_antrian ?></h2></td>
                  </tr>
                  <tr>
                    <td>Loket</td>
                    
                    <td><b><?php echo get_data('loket','id_loket',$antrian->row()->id_loket,'loket') ?></b></td>
                  </tr>
                  <tr>
                    <td>No KTP</td>
                    
                    <td><?php echo $antrian->row()->no_ktp ?></td>
                  </tr>
                  <tr>
                    <td>Nama Lengkap</td>
                    
                    <td><?php echo $antrian->row()->nama ?></td>
                  </tr>
                  <tr>
                    <td>No HP</td>
                    
                    <td><?php echo $antrian->row()->no_hp ?></td>
                  </tr>
                  <tr>
                    <td>Jenis Layanan</td>
                    
                    <td><?php echo get_data('jenis_layanan','id_jenis_layanan',$antrian->row()->id_jenis_layanan,'jenis_layanan') ?></td>
                  </tr>
                  <tr>
                    <td>Waktu Kunjungan</td>
                    
                    <td><?php echo $antrian->row()->waktu_kunjungan ?></td>
                  </tr>

                  <tr>
                    <td>Status Antrian</td>
                    <td>
                      <?php if ($antrian->row()->date_panggil != null): ?>
                        <span class="badge badge-success">Sudah Dipanggil</span> pada jam <span class="badge badge-info"><?php echo $antrian->row()->date_panggil ?></span>
                      <?php else: ?>
                        <span class="badge badge-warning">Belum Dipanggil</span>
                      <?php endif ?>
                    </td>
                  </tr>

                </table>

              <?php else: ?>
                <div class="alert alert-warning" align="center">Maaf, No Antrian tidak ditemukan
              </div>
              <?php endif ?>
              
            <?php endif ?>
          

        </div>

      </div>
    </section><!-- End Contact Section -->

    <!-- ======= Contact Section ======= -->
    <section id="pendaftaran" class="contact section-bg">
      <div class="container">

        <div class="section-title" data-aos="fade-up">
          <h2>Pendaftaran No Antrian</h2>
        </div>

        <div class="row">

          <div class="col-lg-12 mt-5 mt-lg-0 d-flex align-items-stretch" data-aos="fade-left">
            <form action="web/simpan_pendaftaran" method="POST"  class="php-email-form">

              <div class="form-group">
                <label>Pilih Waktu Kujungan</label>
                <select class="form-control" name="waktu_kunjungan" required>
                  <option value="">Pilih</option>
                  <?php foreach ($this->db->get('waktu_kunjungan')->result() as $br): ?>
                  <option value="<?php echo $br->waktu_kunjungan ?>"><?php echo $br->waktu_kunjungan ?></option>
                  <?php endforeach ?>
                </select>
              </div>
            
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="name">No KTP</label>
                  <input type="text" name="no_ktp" pattern="[0-9]+" class="form-control" minlength="16" maxlength="16" required />
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama" required/>
                </div>
              </div>

              <div class="form-group">
                <label for="name">No HP</label>
                <input type="text" class="form-control" name="no_hp" required/>
              </div>
              <div class="form-group">
                <label for="name">Jenis Pelayanan</label>
                <select class="form-control" name="jenis_layanan">
                  <option value="">Pilih Jenis Pelayanan</option>
                  <?php foreach ($this->db->get('jenis_layanan')->result() as $rw): ?>
                  <option value="<?php echo $rw->id_jenis_layanan ?>"><?php echo $rw->jenis_layanan ?></option>
                  <?php endforeach ?>
                </select>
              </div>

              <div class="alert alert-info">
                
                    <b>Dengan ini saya menyatakan bahwa saya setuju dengan aturan yang berlaku ketika mengambil antrian ini.</b>
                <div class="form-group options">    
                    <input type="checkbox" name="setuju" value="A" required /> Saya Setuju
                </div>
              </div>
              <div class="form-group">
                  <button class="btn btn-primary">Daftar</button>
              </div>
              
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->



  <!-- ======= Footer ======= -->
  <?php $this->load->view('antrian_online/footer'); ?>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      
    });
  </script>

</body>

</html>