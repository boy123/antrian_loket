<?php 
date_default_timezone_set('Asia/Jakarta');
$tgl = date('Y-m-d');
$id_loket = $this->session->userdata('id_loket');
$suara = get_data('loket','id_loket',$id_loket,'suara');
$id_jenis_layanan = get_data('loket','id_loket',$id_loket,'id_jenis_layanan');
$jenis_layanan = $this->db->get_where('jenis_layanan', ['id_jenis_layanan'=>$id_jenis_layanan])->row();
$instansi = $this->db->get_where('instansi', ['id_instansi'=>$jenis_layanan->id_instansi])->row();

$berlangsung = 0;
$antrian_now = $this->db->query("SELECT no_antrian FROM antrian where id_loket='$id_loket' and tanggal='$tgl' and date_panggil is not null ORDER BY no_antrian DESC");
if ($antrian_now->num_rows() > 0) {
    $berlangsung = $antrian_now->row()->no_antrian;
}
$antrian = $berlangsung;

//cek sisa antrian belum di panggil
$sisa = 0;
$cek_sisa = $this->db->query("SELECT no_antrian FROM antrian where id_loket='$id_loket' and tanggal='$tgl' and date_panggil is null ");
if ($cek_sisa->num_rows() > 0) {
    $sisa = $cek_sisa->num_rows();
}

//antrian online
$antrian_online = $this->db->query("SELECT * FROM antrian where id_loket='$id_loket' and tanggal='$tgl' and is_online='y' ORDER BY no_antrian DESC");

//total online
$total_antrian = $this->db->query("SELECT no_antrian FROM antrian where id_loket='$id_loket' and tanggal='$tgl' ORDER BY no_antrian DESC");


 ?>

<div class="row">
	<div class="col-md-12">
		<div class="callout callout-info">
			<h4>Selamat Datang kembali, <?php echo $this->session->userdata('username'); ?></h4>
		</div>
	</div>
</div>
	
<div class="row">
	<div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3><?php echo $sisa ?></h3>
          <p>Sisa Antrian</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
      </div>
    </div>

    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3><?php echo $antrian_now->num_rows() ?></h3>
          <p>Antrian Diselesaikan</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        
      </div>
    </div>
    <!-- ./col -->
    
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3><?php echo $antrian_online->num_rows() ?></h3>
          <p>Antrian Online</p>
        </div>
        <div class="icon">
          <i class="ion ion-person"></i>
        </div>
        
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-blue">
        <div class="inner">
          <h3><?php echo $total_antrian->num_rows() ?></h3>
          <p>Total Antrian Hari ini</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
      </div>
    </div>
    <!-- ./col -->
  </div>

<div class="row">
	<div class="col-md-4">
		<div class="box">
            <div class="box-body">
            	<div class="row">
            		<div class="col-md-12">
            			<h4><?php echo strtoupper(get_data('loket','id_loket',$id_loket,'loket')) ?></h4>
            		</div>
            		<hr style="color: lightblue; border: solid 1px; margin-left: 12px; margin-right: 12px;">
            		<div class="col-md-12" style="margin-bottom: 20px">
            			<div class="row">
            				<div class="col-md-6">
            					<span class="info-box-icon bg-aqua"><i class="ion ion-android-alarm-clock"></i></span>
            				</div>
            				<div class="col-md-6" style="text-align: right;">
            					<h3 id="jam"><?php echo get_jam() ?></h3>
            					<?php
            					 echo hari_id($tgl).', '.tanggal_indo($tgl) ?>
            				</div>
            			</div>
            		</div>
            		<hr style="color: lightblue; border: solid 1px; margin-left: 12px; margin-right: 12px;">
            		<div class="col-md-12">
            			<div class="row">
            				<div class="col-md-12" style="text-align: left;">
            					<h4><?php echo $jenis_layanan->jenis_layanan ?></h4>
            					<h6><?php echo $instansi->nama_instansi ?></h6>
            				</div>
            			</div>
            		</div>
            	</div>
            </div>
        </div>
	</div>
	<div class="col-md-8">
		<div class="box">
            <div class="box-body">
            	<div class="row">
            		<div class="col-md-8">
            			<div class="alert alert-info">
            				<center>
            					<h5>ANTRIAN SEKARANG</h5>
            					<h1 style="font-size: 95px"><?php echo $berlangsung ?></h1>
            				</center>
            			</div>
            		</div>
            		<div class="col-md-2"></div>
            		<div class="col-md-2">
            			<div class="row">
            				<div class="col-md-12" style="margin-bottom: 10px">
            					<a href="app/next_antrian/<?php echo $id_loket ?>" class="btn btn-primary btn-block"><i class="fa fa-play"></i> NEXT</a>
            				</div>
            				<div class="col-md-12">
            					<button class="btn btn-warning btn-block" onclick="update('1')"><i class="fa fa-rotate-left"></i> PANGGIL</button>
            				</div>
            			</div>
            		</div>
            	</div>
            </div>
        </div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header" style="text-align: center;">
                <h3 class="box-title">Antrian Online yang masuk hari ini</h3>
            </div>
	        <div class="box-body">
	        	<table class="table table-bordered" id="example2">
					<thead>
						<tr>
							<th>#</th>
							<th>Nomor Antrian</th>
							<th>Nama</th>
							<th>No KTP</th>
							<th>No HP</th>
							<th>Waktu Kunjungan</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 1;
						 foreach ($antrian_online->result() as $rw): ?>
						<tr>
							<td><?php echo $no ?></td>
							<td><?php echo $rw->no_antrian ?></td>
							<td><?php echo $rw->nama ?></td>
							<td><?php echo $rw->no_ktp ?></td>
							<td><?php echo $rw->no_hp ?></td>
							<td><?php echo $rw->waktu_kunjungan ?></td>
						</tr>
						<?php $no++; endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script src="https://www.gstatic.com/firebasejs/7.15.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.15.0/firebase-database.js"></script>
<script type="text/javascript">

	var firebaseConfig = {
    apiKey: "AIzaSyAkzFs2A4XPJP7GnOfOq-XgkekECyqUMa0",
    authDomain: "crud-web-be585.firebaseapp.com",
    databaseURL:
      "https://crud-web-be585-default-rtdb.asia-southeast1.firebasedatabase.app/",
    projectId: "crud-web-be585",
    storageBucket: "crud-web-be585.appspot.com",
    messagingSenderId: "214105427344",
    appId: "1:214105427344:web:34a24e2b574a572f8a3715",
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  var database = firebase.database();

  function update(key) {
    database.ref("notifikasi/" + key).update({ 
      id_loket: "<?php echo $id_loket ?>",
      no_antrian: "<?php echo $antrian ?>",
      timestamp: Date.now()
    });
  }

	$(function(){
		$("#play").click(function(){
			document.getElementById('suarabel').play();
			document.getElementById('suarabelnomorurut').play();
			document.getElementById('suarabelsuarabelloket').play();
		});

		$("#pause").click(function(){
			document.getElementById("suarabel").pause();
		});

		$("#stop").click(function(){
			document.getElementById("suarabel").pause();
			document.getElementById("suarabel").currentTime=0;
		});


	})
</script>
<audio id="suarabel" src="<?php echo base_url('audio/Airport_Bell.mp3'); ?>"></audio>
<audio id="suarabelnomorurut" src="<?php echo base_url('audio/antrian/nomor-urut.wav'); ?>"></audio> 
<audio id="diloket" src="<?php echo base_url('audio/antrian/loket.wav'); ?>"></audio>
<?php
if($antrian > 11 && $antrian < 20){ ?>
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.substr($antrian, -1,1).'.wav'); ?>"></audio>
<?php }
else if($antrian == 20){ ?>
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.substr($antrian, 0,1).'.wav'); ?>"></audio>
<?php }
else if($antrian > 20 && $antrian < 100){ ?> 
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.substr($antrian, 0,1).'.wav'); ?>"></audio>
	<?php
	$a=substr($antrian, -1,1);
	if($a == 0){

	}
	else{?>
		<audio id="antrian1" src="<?php echo base_url('audio/antrian/'.$a.'.wav'); ?>"></audio>
<?php 
	}
}
else if($antrian > 100 && $antrian < 110){ ?>
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.substr($antrian, -1,1).'.wav'); ?>"></audio>
<?php 
}
else if($antrian > 111 && $antrian < 120){ ?>
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.substr($antrian, -1,1).'.wav'); ?>"></audio>
<?php 
 
}
else if($antrian > 119 && $antrian < 210){ ?>
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.substr($antrian, 0,1).'.wav'); ?>"></audio>
	<?php
	$a=substr($antrian, -1,1);
	if($a == 0){

	}
	else{?>
		<audio id="antrian1" src="<?php echo base_url('audio/antrian/'.$a.'.wav'); ?>"></audio>
<?php 
	}
}
else if($antrian == 210){ ?>
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.substr($antrian, 0,1).'.wav'); ?>"></audio>
<?php }
else if($antrian == 211){ ?>
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.substr($antrian, 0,1).'.wav'); ?>"></audio>
<?php }
else if($antrian > 211 && $antrian < 220){ ?>
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.substr($antrian, 0,1).'.wav'); ?>"></audio>
	<audio id="antrian1" src="<?php echo base_url('audio/antrian/'.substr($antrian, -1,1).'.wav'); ?>"></audio>
<?php 
}
else if($antrian > 219 && $antrian < 1000){ ?> 
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.substr($antrian, 0,1).'.wav'); ?>"></audio>
<?php
	$a = substr($antrian, 1,1);
	$b = substr($antrian, -1,1);
		echo "<audio id='antrian1' src='".base_url('audio/antrian/'.$a).".wav'></audio>";
		echo "<audio id='antrian2' src='".base_url('audio/antrian/'.$b).".wav'></audio>";
}
else{ ?>
	<audio id="antrian" src="<?php echo base_url('audio/antrian/'.$antrian.'.wav'); ?>"></audio>
<?php } ?>

<audio id="loket<?php echo $id_loket; ?>" src="<?php echo base_url('audio/loket/'.$suara); ?>"></audio>
<audio id="sepuluh" src="<?php echo base_url('audio/antrian/sepuluh.wav'); ?>"></audio>
<audio id="sebelas" src="<?php echo base_url('audio/antrian/sebelas.wav'); ?>"></audio>
<audio id="seratus" src="<?php echo base_url('audio/antrian/seratus.wav'); ?>"></audio>
<audio id="belas" src="<?php echo base_url('audio/antrian/belas.wav'); ?>"></audio>
<audio id="puluh" src="<?php echo base_url('audio/antrian/puluh.wav'); ?>"></audio>
<audio id="ratus" src="<?php echo base_url('audio/antrian/ratus.wav'); ?>"></audio>

<script type="text/javascript">
		function panggil(){
			document.getElementById("suarabel").pause();
			document.getElementById("suarabel").currentTime=0;
			document.getElementById("suarabel").play();

			//set delay
			totalWaktu = document.getElementById("suarabel").duration*1000;

			//playnomerurutnya
			setTimeout(function(){
				document.getElementById("suarabelnomorurut").pause()
				;document.getElementById("suarabelnomorurut").currentTime=0;
				document.getElementById("suarabelnomorurut").play();
			}, totalWaktu);
			totalWaktu=totalWaktu+1000;

			<?php
			if($antrian < 10){ ?>
				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+1000;
			<?php }
			elseif($antrian == 10){ ?>
				setTimeout(function(){
					document.getElementById("sepuluh").pause();
					document.getElementById("sepuluh").currentTime=0;
					document.getElementById("sepuluh").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+1000;
			<?php }

			elseif($antrian == 11){ ?>
				setTimeout(function(){
					document.getElementById("sebelas").pause();
					document.getElementById("sebelas").currentTime=0;
					document.getElementById("sebelas").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+1000;
			
			<?php } 

			else if($antrian > 11 && $antrian < 20){ 
				?>
				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+1000;

				setTimeout(function(){
					document.getElementById("belas").pause();
					document.getElementById("belas").currentTime=0;
					document.getElementById("belas").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+1000;
			
			<?php }

			else if($antrian == 20){ 
				?>
				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("puluh").pause();
					document.getElementById("puluh").currentTime=0;
					document.getElementById("puluh").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+1000;
			
			<?php }
			else if($antrian > 20 && $antrian < 100){ 
				?>
				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("puluh").pause();
					document.getElementById("puluh").currentTime=0;
					document.getElementById("puluh").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+1000;

				setTimeout(function(){
					document.getElementById("antrian1").pause();
					document.getElementById("antrian1").currentTime=0;
					document.getElementById("antrian1").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		else if($antrian == 100){ ?>

				setTimeout(function(){
					document.getElementById("seratus").pause();
					document.getElementById("seratus").currentTime=0;
					document.getElementById("seratus").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		

		<?php
		}
		else if ($antrian > 100 && $antrian < 110) { ?>
				setTimeout(function(){
					document.getElementById("seratus").pause();
					document.getElementById("seratus").currentTime=0;
					document.getElementById("seratus").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		else if($antrian == 110){ ?>

				setTimeout(function(){
					document.getElementById("seratus").pause();
					document.getElementById("seratus").currentTime=0;
					document.getElementById("seratus").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		
				setTimeout(function(){
					document.getElementById("sepuluh").pause();
					document.getElementById("sepuluh").currentTime=0;
					document.getElementById("sepuluh").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		else if($antrian == 111){ ?>

				setTimeout(function(){
					document.getElementById("seratus").pause();
					document.getElementById("seratus").currentTime=0;
					document.getElementById("seratus").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		
				setTimeout(function(){
					document.getElementById("sebelas").pause();
					document.getElementById("sebelas").currentTime=0;
					document.getElementById("sebelas").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		else if($antrian > 111 && $antrian < 120){ ?>

				setTimeout(function(){
					document.getElementById("seratus").pause();
					document.getElementById("seratus").currentTime=0;
					document.getElementById("seratus").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
				
				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("belas").pause();
					document.getElementById("belas").currentTime=0;
					document.getElementById("belas").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		else if($antrian > 119 && $antrian < 200){ 
				?>
				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("antrian1").pause();
					document.getElementById("antrian1").currentTime=0;
					document.getElementById("antrian1").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("puluh").pause();
					document.getElementById("puluh").currentTime=0;
					document.getElementById("puluh").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("antrian1").pause();
					document.getElementById("antrian1").currentTime=0;
					document.getElementById("antrian1").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		else if($antrian > 199 && $antrian < 210){ 
				?>
				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("ratus").pause();
					document.getElementById("ratus").currentTime=0;
					document.getElementById("ratus").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("antrian1").pause();
					document.getElementById("antrian1").currentTime=0;
					document.getElementById("antrian1").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		else if($antrian == 210){ 
				?>
				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("ratus").pause();
					document.getElementById("ratus").currentTime=0;
					document.getElementById("ratus").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("sepuluh").pause();
					document.getElementById("sepuluh").currentTime=0;
					document.getElementById("sepuluh").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		else if($antrian == 211){ ?>

				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		
				setTimeout(function(){
					document.getElementById("ratus").pause();
					document.getElementById("ratus").currentTime=0;
					document.getElementById("ratus").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("sebelas").pause();
					document.getElementById("sebelas").currentTime=0;
					document.getElementById("sebelas").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		else if($antrian > 211 && $antrian < 220){ ?>

				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
				
				setTimeout(function(){
					document.getElementById("ratus").pause();
					document.getElementById("ratus").currentTime=0;
					document.getElementById("ratus").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("antrian1").pause();
					document.getElementById("antrian1").currentTime=0;
					document.getElementById("antrian1").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("belas").pause();
					document.getElementById("belas").currentTime=0;
					document.getElementById("belas").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		else if($antrian > 219 && $antrian < 1000){ ?>

				setTimeout(function(){
					document.getElementById("antrian").pause();
					document.getElementById("antrian").currentTime=0;
					document.getElementById("antrian").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
				
				setTimeout(function(){
					document.getElementById("ratus").pause();
					document.getElementById("ratus").currentTime=0;
					document.getElementById("ratus").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("antrian1").pause();
					document.getElementById("antrian1").currentTime=0;
					document.getElementById("antrian1").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("puluh").pause();
					document.getElementById("puluh").currentTime=0;
					document.getElementById("puluh").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;

				setTimeout(function(){
					document.getElementById("antrian2").pause();
					document.getElementById("antrian2").currentTime=0;
					document.getElementById("antrian2").play();
				}, totalWaktu);
				totalWaktu=totalWaktu+800;
		<?php
		}
		?>

			totalwaktu=totalWaktu+1000;
			setTimeout(function() {
							document.getElementById('diloket').pause();
							document.getElementById('diloket').currentTime=0;
							document.getElementById('diloket').play();
						}, totalwaktu);
			
			totalwaktu=totalwaktu+1000;
			setTimeout(function() {
							document.getElementById('loket<?php echo $id_loket; ?>').pause();
							document.getElementById('loket<?php echo $id_loket; ?>').currentTime=0;
							document.getElementById('loket<?php echo $id_loket; ?>').play();
						}, totalwaktu);	
		}

		function get_jam() {
			$.ajax({
            url: 'app/tgl_jam_today',
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            $("#jam").text(data.jam);
        })
        .fail(function() {
            console.log("error");
        })
		}

		setInterval(function() {
			get_jam();
		}, 1000);


	</script>


