
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Waktu Kunjungan <?php echo form_error('waktu_kunjungan') ?></label>
            <input type="text" class="form-control" name="waktu_kunjungan" id="waktu_kunjungan" placeholder="Waktu Kunjungan" value="<?php echo $waktu_kunjungan; ?>" />
        </div>
	    <input type="hidden" name="id_waktu_kunjungan" value="<?php echo $id_waktu_kunjungan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('waktu_kunjungan') ?>" class="btn btn-default">Cancel</a>
	</form>
   