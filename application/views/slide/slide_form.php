
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
	    <div class="form-group">
            <label for="varchar">Slide <?php echo form_error('slide') ?></label>
            <input type="file" class="form-control" name="slide" required />
            <input type="hidden" name="slide_old" value="<?php echo $slide ?>">
            <p>
                <?php if ($slide != ''): ?>
                    *) Gambar sebelumnya <br>
                    <img src="image/slide/<?php echo $slide ?>" style="height: 200px;">
                <?php endif ?>
                
            </p>
        </div>
	    <input type="hidden" name="id_slide" value="<?php echo $id_slide; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('slide') ?>" class="btn btn-default">Cancel</a>
	</form>
   