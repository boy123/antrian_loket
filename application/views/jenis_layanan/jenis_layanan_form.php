
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Instansi <?php echo form_error('id_instansi') ?></label>
            <select name="id_instansi" class="form-control select2">
                <option value="<?php echo $id_instansi ?>"><?php echo get_data('instansi','id_instansi',$id_instansi,'nama_instansi') ?></option>
                <?php foreach ($this->db->get('instansi')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_instansi ?>"><?php echo $rw->nama_instansi ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="varchar">Jenis Layanan <?php echo form_error('jenis_layanan') ?></label>
            <input type="text" class="form-control" name="jenis_layanan" id="jenis_layanan" placeholder="Jenis Layanan" value="<?php echo $jenis_layanan; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Max Antrian <?php echo form_error('max_antrian') ?></label>
            <input type="text" class="form-control" name="max_antrian" id="max_antrian" placeholder="Max Antrian" value="<?php echo $max_antrian; ?>" />
        </div>
	    <div class="form-group">
            <label for="keterangan">Keterangan <?php echo form_error('keterangan') ?></label>
            <textarea class="form-control" rows="3" name="keterangan" id="keterangan" placeholder="Keterangan"><?php echo $keterangan; ?></textarea>
        </div>
	    <input type="hidden" name="id_jenis_layanan" value="<?php echo $id_jenis_layanan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('jenis_layanan') ?>" class="btn btn-default">Cancel</a>
	</form>
   